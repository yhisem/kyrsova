/**
 * Created by Yon on 03.04.14.
 */
function openCategoryPopup(url,option,collor)
{
    if(collor)
    {
        var backGround = collor;
    }
    else
    {
        var backGround = '#ffffff';
    }
    var popup = '<div class="popup"><div class="container loader" style="background-color: '+backGround+'"><div class="close"></div><div class="content"></div></div></div>';
    $('body').prepend(popup);

    $.ajax({
        url: url,
        data: {id:option.id},
        type: "POST",
        dataType: 'json'
    }).done(function (data) {
        $('.popup .container').removeClass('loader');
        $('.popup .content').html(data.view);
    }).error(function (error) {
        alert(error.messages)
    });


    $('.close').on('click',function()
    {
        var $this = $(this);
        $this.parent().parent().remove();
    });
}