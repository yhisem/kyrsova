<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ert
 * Date: 4/28/13
 * Time: 5:11 PM
 * To change this template use File | Settings | File Templates.
 */

class I18nControllerBehavior extends CBehavior
{
    public function createMultilanguageReturnUrl($lang = 'en')
    {
        if (count($_GET) > 0) {
            $arr = $_GET;
            $arr['language'] = $lang;
        } else
            $arr = array('language' => $lang);
        return $this->getOwner()->createUrl('', $arr);
    }

    var $language=array();

    public function getBestMatch($default, $langs)
    {
        $languages=array();
        foreach ($langs as $lang => $alias)
        {
            if (is_array($alias) && isset($alias['substitute']) && is_array($alias['substitute']))
            {
                foreach ($alias['substitute'] as $alias_lang)
                {
                    $languages[strtolower($alias_lang)] = strtolower($lang);
                }
            }
            $languages[strtolower($lang)] = strtolower($lang);
        }

        foreach ($this->language as $l => $v) {
            $s = strtok($l, '-'); // убираем то что идет после тире в языках вида "en-us, uk-uk"
            if (isset($languages[$s]))
                return $languages[$s];
        }
        return $default;
    }
    /**
     * @param CComponent $owner
     */
    public function attach($owner)
    {
        parent::attach($owner);

        if (isset($_POST['language']))
        {
            $lang = $_POST['language'];
            Yii::app()->user->setState('language', $lang);
            setcookie("language",$lang, time() + (60 * 60 * 24 * 365), "/");
            $MultilangReturnUrl = $_POST[$lang];
            $this->redirect($MultilangReturnUrl);
        }
        else if (isset($_GET['language']))
        {
            Yii::app()->language = $_GET['language'];
        }
        else if (Yii::app()->user->hasState('language'))
            Yii::app()->language = Yii::app()->user->getState('language');
        else if (isset(Yii::app()->request->cookies['language']))
            Yii::app()->language = Yii::app()->request->cookies['language']->value;
        else if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']))
        {
            $list = strtolower($_SERVER['HTTP_ACCEPT_LANGUAGE']);
            if (preg_match_all('/([a-z]{1,8}(?:-[a-z]{1,8})?)(?:;q=([0-9.]+))?/', $list, $list)) {
                $this->language = array_combine($list[1], $list[2]);
                foreach ($this->language as $n => $v)
                    $this->language[$n] = $v ? $v : 1;
                arsort($this->language, SORT_NUMERIC);
            }

            $language=$this->getBestMatch(Yii::app()->language, Yii::app()->params['languages']);
            Yii::app()->language = $language;
        }
        else
        {
            $lang = Yii::app()->language;
            if (empty($lang))
                $lang = Yii::app()->sourceLanguage;

            if (empty($lang))
                $lang = "en";

            Yii::app()->language = $lang;
        }

        Yii::app()->user->setState('language', Yii::app()->language);
        setcookie("language",Yii::app()->language, time() + (60 * 60 * 24 * 365), "/");
   }
}