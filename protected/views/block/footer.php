<footer>
    <div class="container_24">
        <div class="wrapper p6">
            <article class="grid_7 spacer-1 maxheight">
                <h4 class="color-1 p0"><?=t('main','Сторінки')?></h4>
                <ul class="list-1">
                    <li><?=CHtml::link(t('main','Новые ветры'),array('/infoPage/view/1' , 'language'=>Yii::app()->language) ) ?></li>
                    <li><?=CHtml::link(t('main','Рисковая работа'),array('/infoPage/view/1' , 'language'=>Yii::app()->language) ) ?></li>
                    <li><?=CHtml::link(t('main','Экология'),array('/infoPage/view/1' , 'language'=>Yii::app()->language) ) ?></li>
                    <li><?=CHtml::link(t('main','Клуб "Экстрим"'),array('/infoPage/view/1' , 'language'=>Yii::app()->language) ) ?></li>
                    <li><?=CHtml::link(t('main','Кто культурнее?'),array('/infoPage/view/1' , 'language'=>Yii::app()->language) ) ?></li>
                    <li><?=CHtml::link(t('main','В прошлой жизни'),array('/infoPage/view/1' , 'language'=>Yii::app()->language) ) ?></li>
                </ul>
            </article>
            <article class="grid_10 spacer-2 maxheight">
                <h4 class="color-1 p0"><?=t('block','О нас пишут')?>:</h4>
                <ul class="list-services wrapper">
                    <li><?= CHtml::link('<div class="write type-1"></div>','http://blogs.lb.ua/alla_tsvetkova',array('target'=>"_blank"))?></li>
                    <li><?= CHtml::link('<div class="write type-2"></div>','http://www.aif.ua/health/964822',array('target'=>"_blank"))?></li>
                    <li><?= CHtml::link('<div class="write type-3"></div>','http://old.natali.ua/materials/show.html?p=5&id=2787',array('target'=>"_blank"))?></li>
                    <li><?= CHtml::link('<div class="write type-4"></div>','http://vn.20minut.ua/Zdorovya/159501',array('target'=>"_blank"))?></li>
                </ul>
                <h4 class="color-1 p0" style="display: inline-block; top: -10px;position: relative;"><?=t('block','Поделиться')?>:</h4>\
                <div class="share42init" style="display: inline-block"></div>
                <script type="text/javascript" src="/js/share42/share42.js"></script>
            </article>
            <article class="grid_7 maxheight feedback-footer">
                <?$this->renderPartial('//block/feedback')?>
            </article>
        </div>
    </div>
    <div class="footer-row">
        <strong>© 2013 - <?=date("Y")?> Юрсана - клуб здорового образа жизни. Все права защищены. <?= CHtml::link('yursana.com','http://yursana.com/',array('class'=>'link-for-site'))?>.</strong>
    </div>
</footer>