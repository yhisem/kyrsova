<?php

class SiteTest extends WebTestCase
{
	public function testIndex()
	{
		$this->open('');
        $this->assertTextPresent('Свежие');
        $this->assertTextPresent('комментарии');
        $this->assertTextPresent('Лучшие статьи');
        $this->assertTextPresent('Список категорий');
        $this->assertTextPresent('Цитата дня');
        $this->assertTextPresent('Еще проекты:');

        $this->assertTextPresent('Всего понемногу');
        $this->assertTextPresent('Вход');
        $this->assertTextPresent('Регистрация');
        $this->close();
    }

	public function testContact()
	{
		$this->open('infopage/about');
        $this->assertTextPresent('Про LifeGid');
        $this->assertTextPresent('довольно долго лежал на полке и');
//		$this->assertElementPresent('name=ContactForm[name]');
//
//		$this->type('name=ContactForm[name]','tester');
//		$this->type('name=ContactForm[email]','tester@example.com');
//		$this->type('name=ContactForm[subject]','test subject');
//		$this->click("//input[@value='Submit']");
//		$this->waitForTextPresent('Body cannot be blank.');
        $this->close();
	}

	public function testLoginLogout()
	{
		$this->open('');
		// ensure the user is logged out
		if($this->isElementPresent("link=Выход"))
			$this->click('link=Выход');

		// test login process, including validation
		$this->click('link=Вход');
		$this->assertElementPresent('name=LoginForm[login]');
		$this->type('name=LoginForm[login]','t21@ert.org.ua');
		$this->clickAndWait("//button[@id='loginButton']");
        $this->assertTextNotPresent('Выход');
//		$this->waitForTextPresent('Password cannot be blank.');
		$this->type('name=LoginForm[password]','123456');
		$this->clickAndWait("//button[@id='loginButton']");
//		$this->assertTextNotPresent('Password cannot be blank.');
		$this->assertTextPresent('Выход');

		// test logout process
		$this->assertElementNotPresent('css=.login-click.login');
		$this->clickAndWait('link=Выход');
        $this->assertElementPresent('css=.login-click.login');
        $this->close();
	}
}
