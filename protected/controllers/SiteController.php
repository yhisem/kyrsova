<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{

		return array(

		);
	}

    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'views' actions
                'actions'=>array('index', 'categoryList', 'error', 'test', 'info'),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array(''),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionInfo()
    {

        $model = new UserInfo();
        if(isset($_POST['user']) && !empty($_POST['user']))
        {
            $model->user = $_POST['user'];
            $model->sex = $_POST['sex'];
            $model->birthday = $_POST['birthday'];
            $model->height = $_POST['height'];
            $model->weight = $_POST['weight'];
            if($model->save())
            {
                echo json_encode(array('massage'=>t('main','Мы получили ваши данные и в скором времени с вами свяжимся')));
                Yii::app()->end();
            }
        }
        $view = $this->renderPartial('info',array(
            'model' => $model,
        ),true);
        echo json_encode(array('view'=>$view));
    }

    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $criteria->condition = 'parentId = :parentId AND language = :language';
        $criteria->params = array(':parentId'=>0 , ':language'=>Yii::app()->language);
        $category = Category::model()->findAll($criteria);

        $sliderSql = " SELECT S.* , SS.submenu FROM Slider S
                        LEFT JOIN (SELECT GROUP_CONCAT(SL.title,'|',SL.url) submenu , SL.place FROM Slider SL
                        WHERE SL.parent = 1 AND SL.`language` = :lang GROUP BY SL.place)
                            SS ON SS.place = S.place
                            WHERE S.parent = 0 AND S.`language` = :lang ";
        $param = array();
        $param[':lang'] = Yii::app()->language;
        $slider = Yii::app()->db->createCommand(" $sliderSql ")->queryAll(true,$param);

        $this->render('index',array(
            'category'=>$category,
            'slider'=>$slider
        ));
    }

    public function actionCategoryList()
    {
        if($_POST['id'])
        {
            $idCategoryType = $_POST['id'];
            $criteria = new CDbCriteria();
            $criteria->condition = ('id = :id AND language = :language');
            $criteria->params = array(':id'=>$idCategoryType , ':language'=>Yii::app()->language);
            $model = Category::model()->find($criteria);
            $criteria = new CDbCriteria();
            $criteria->condition = 'parentId = :parentId AND language = :language';
            $criteria->params = array(':parentId'=>$model->id, ':language'=>Yii::app()->language);
            $category = Category::model()->findAll($criteria);



            $view = $this->renderPartial('categoryView',array(
                'category' => $category,
                'model'=>$model
            ),true);
          echo json_encode(array('view'=>$view));
        }
    }

    public function actionError()
    {
        $this->layout = "main";
        if($error=Yii::app()->errorHandler->error)
        {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    public function actionTest(){


//        $this->layout = 'test';
//        $this->render('slider',array(
//            'slider'=>$slider,
//            'activeId'=>1
//        ));
    }

}